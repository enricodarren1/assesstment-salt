import '../css/home.css';
import '../css/spacer.css';
import banner from '../assets/images/banner.svg';
import scrollArrow from '../assets/images/scroll-arrow.svg';
import iconCompany from '../assets/images/icon-company.svg';
import arrowPrev from '../assets/icons/arrow-prev.svg';
import arrowNext from '../assets/icons/arrow-next.svg';
import coreValuesImage from '../assets/images/core-values-image.svg';
import speciality1 from '../assets/images/speciality-1.svg';
import speciality2 from '../assets/images/speciality-2.svg';
import speciality3 from '../assets/images/speciality-3.svg';
import iconCompanyWhite from '../assets/images/icon-company-white.svg';

import { Navigation, Pagination, Scrollbar, A11y, EffectFade } from 'swiper';
import { Swiper, SwiperSlide, useSwiper } from "swiper/react";

import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import React, { useState } from 'react';


function Home() {

  var [indexPageSection1, setIndexPageSection1] = useState(0);

  const contentSection1 = [
    {
      title: 'Technology Company',
      description: 'Sed ut perspiciatis unde omnis iste natus sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'
    },
    {
      title: 'Professional Brand Management',
      description: 'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.'
    },
    {
      title: 'Strategize, Design, Collaborate',
      description: 'Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse sequam nihil molestiae consequatur.'
    }
  ]

  const scrollToSection1 = () => {
    document
      .querySelector('.section-1')
      .scrollIntoView({
        behavior: 'smooth',
      });
  };

  const slideNextSection1 = () => {
    if (indexPageSection1 < contentSection1.length - 1) {
      indexPageSection1++;
      setIndexPageSection1(indexPageSection1);
    }
  }

  const slidePrevSection1 = () => {
    if (indexPageSection1 < contentSection1.length && indexPageSection1 > 0) {
      indexPageSection1--;
      setIndexPageSection1(indexPageSection1);
    }
  }

  return (
    <div className='home'>
      <header>
        <div className='header'>
          <img src={iconCompany} alt='icon company' />
        </div>
      </header>

      <div>
        <div>
          <img src={banner} className="background-image-banner" alt="background top" />
        </div>

        <div className='text-center'>
          <img src={scrollArrow} className="scroll-arrow" alt="scroll arrow" onClick={scrollToSection1} />
        </div>

        <div className='section-1'>
          <div className='text-center mb-16'>
            <span className='section-1-title'>Who we are</span>
          </div>
          <div>
            <span className='section-1-sub-title'>{contentSection1[indexPageSection1]?.title}</span>
          </div>
          <div className='mb-40'>
            <span className='section-1-description'>{contentSection1[indexPageSection1]?.description}</span>
          </div>

          <div className='pagination-section-1'>
            <div className='row'>
              <div className='col text-start'>
                <span className='section-1-pagination'>{indexPageSection1 < 10 ? '0' : ''}{indexPageSection1 + 1}</span><span> / 03</span>
              </div>
              <div className='col text-end'>
                <img src={arrowPrev} className='arrow-pagination-icon' alt='arrow prev' onClick={slidePrevSection1} />
                <img src={arrowNext} className='arrow-pagination-icon' alt='arrow next' onClick={slideNextSection1} />
              </div>
            </div>
          </div>
        </div>

        <div className='section-2'>
          <div className='text-center mb-24'>
            <span className='core-values-title'>Our Core Values</span>
          </div>

          <div className='mb-34'>
            <span className='core-values-description'>
              Ridiculus laoreet libero pretium et, sit vel elementum convallis fames. Sit suspendisse etiam eget egestas. Aliquet odio et lectus etiam sit.
              <br /><br />
              In mauris rutrum ac ut volutpat, ornare nibh diam. Montes, vitae, nec amet enim.
            </span>
          </div>

          <div className='core-values'>
            <ul>
              <li className='mb-24'>
                <div>
                  <span className='core-values-list-title'>Dedication</span>
                </div>
                <div>
                  <span className='core-values-list-description'>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat.</span>
                </div>
              </li>
              <li className='mb-24'>
                <div>
                  <span className='core-values-list-title'>Intellectual Honesty</span>
                </div>
                <div>
                  <span className='core-values-list-description'>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias conse.</span>
                </div>
              </li>
              <li className='mb-24'>
                <div>
                  <span className='core-values-list-title'>Curiosity</span>
                </div>
                <div>
                  <span className='core-values-list-description'>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque.</span>
                </div>
              </li>
            </ul>
          </div>

          <div className='text-end'>
            <img src={coreValuesImage} alt='core values images' className='section-2-image' />
          </div>
        </div>

        <div className='section-3'>
          <div className='our-speciality'>
            <div className='text-center'>
              <span className='our-speciality-title'>Our Speciality</span>
            </div>
            <div className='mb-24 text-center'>
              <span className='our-speciality-sub-title'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.</span>
            </div>
            <div className='swiper-container'>
              <Swiper
                modules={[Pagination, Scrollbar, A11y, EffectFade]}
                spaceBetween={80}
                slidesPerView={1}
                initialSlide={1}
                pagination={{ clickable: true, dynamicBullets: true }}
                scrollbar={{ draggable: true }}
              >
                <SwiperSlide>
                  <div className='text-center'>
                    <div>
                      <img src={speciality1} alt='accesories' />
                    </div>
                    <div>
                      <span className='our-speciality-list-title'>Accesories</span>
                    </div>
                    <div>
                      <span className='our-speciality-list-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.</span>
                    </div>
                  </div>
                </SwiperSlide>
                <SwiperSlide>
                  <div className='text-center'>
                    <div>
                      <img src={speciality2} alt='speed improvement' />
                    </div>
                    <div>
                      <span className='our-speciality-list-title'>Speed Improvement</span>
                    </div>
                    <div>
                      <span className='our-speciality-list-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.</span>
                    </div>
                  </div>
                </SwiperSlide>
                <SwiperSlide>
                  <div className='text-center'>
                    <div>
                      <img src={speciality3} alt='exhaust' />
                    </div>
                    <div>
                      <span className='our-speciality-list-title'>Exhaust</span>
                    </div>
                    <div>
                      <span className='our-speciality-list-description'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis euismod libero vel leo auctor, in venenatis nulla consequat. Sed commodo nunc sit amet congue aliquam.</span>
                    </div>
                  </div>
                </SwiperSlide>
              </Swiper>
            </div>
          </div>
        </div>

        <div className='section-4'>
          <div className='mb-30'>
            <img src={iconCompanyWhite} alt='icon company white' />
          </div>

          <div className='address-container'>
            <div className='custom-select-option-container'>
              <select className='custom-select-option'>
                <option>Technology Department</option>
                <option>Business Department</option>
                <option>Marketing Department</option>
              </select>
            </div>
            <div className='address-location'>
              <span>Jl. Lembong No 8 Kel. Braga Kec. Sumur Bandung, Kota Bandung, Jawa Barat</span>
            </div>
          </div>

          <div className='footer-menu'>
            <div>
              <span className='footer-menu-label'>Who We Are</span>
            </div>
            <div>
              <span className='footer-menu-label'>Our Values</span>
            </div>
            <div>
              <span className='footer-menu-label'>The Perks</span>
            </div>
          </div>
        </div>

      </div>
    </div>
  );
}

export default Home;
